﻿using System;

namespace TestModelBuilderExample.Models
{
    public class Booking
    {
        public string BookingReference { get; }
        public DateTime DateTime { get; }
        public string Surname { get; }

        public Booking(string bookingReference, DateTime dateTime, string surname)
        {
            BookingReference = bookingReference;
            DateTime = dateTime;
            Surname = surname;
        }
    }
}