# Builder Pattern Example
## Components

### **TestModelBuilderExample** project  

**`Models/Booking.cs`**  
An example model, for which we'll create a builder.

**`Services/BookingService.cs`**  
A simple, contrived service with a single method to test.

### **TestModelBuilderExample.Tests** project  

**`Builders/BookingBuilder.cs`**  
The builder that builds our `Booking` instances for testing.

**`BookingServiceTests.cs`**  
The test class that consumes the builder to test the `BookingService`.