﻿using System;
using TestModelBuilderExample.Models;

namespace TestModelBuilderExample.Tests.Builders
{
    /// <summary>
    /// Builds <see cref="Booking"/> models for tests (a demonstration of the Builder pattern for test models).
    /// </summary>
    public class BookingBuilder
    {
        private string _bookingReference = "DEFAULT_REFERENCE";
        private DateTime _dateTime = DateTime.MinValue;
        private string _surname = "DefaultSurname";
        
        public BookingBuilder WithSurname(string surname)
        {
            _surname = surname;
            return this;
        }

        public BookingBuilder WithBookingReference(string bookingReference)
        {
            _bookingReference = bookingReference;
            return this;
        }

        public BookingBuilder WithDateTime(DateTime dateTime)
        {
            _dateTime = dateTime;
            return this;
        }

        public Booking Build()
        {
            return new Booking(_bookingReference, _dateTime, _surname);
        }
    }
}