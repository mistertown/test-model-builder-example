﻿using FluentAssertions;
using NUnit.Framework;
using TestModelBuilderExample.Services;
using TestModelBuilderExample.Tests.Builders;

namespace TestModelBuilderExample.Tests
{
    /// <summary>
    /// A series of simple tests to explain the Builder pattern for test models.
    /// </summary>
    [TestFixture]
    public class BookingServiceTests
    {
        [Test]
        public void ProcessBooking_WhenAllFieldsAreOk_ShouldReturnTrue()
        {
            // Arrange
            // gives us a generic booking for testing (Any reference not starting with 'A' is valid for the context of this example)
            var booking = new BookingBuilder().Build();

            var bookingService = new BookingService();
            
            // Act
            var result = bookingService.ProcessBooking(booking);

            // Assert
            result.Should().BeTrue();
        }

        [Test]
        public void ProcessBooking_WhenBookingReferenceIsNotAsExpected_ShouldReturnFalse()
        {
            // Arrange
            // gives us a booking with the specific field set as we want.
            // this gives us clear context within the test of which fields affect the outcome
            // and doesn't pollute this test with other properties that we're not really interested in right now.
            var booking = new BookingBuilder()
                                .WithBookingReference("AAAAA")
                                .Build();

            var bookingService = new BookingService();
            
            // Act
            var result = bookingService.ProcessBooking(booking);

            // Assert
            result.Should().BeFalse();
        }

        [Test]
        public void ProcessBooking_WhenSurnameIsNotAsExpected_ShouldReturnFalse()
        {
            // Arrange
            // gives us a booking with the specific field set as we want - this time we leave the others as defaults and specify Surname
            var booking = new BookingBuilder()
                .WithSurname("Townsend")
                .Build();
            
            var bookingService = new BookingService();

            // Act
            var result = bookingService.ProcessBooking(booking);

            // Assert
            result.Should().BeFalse();
        }
    }
}
