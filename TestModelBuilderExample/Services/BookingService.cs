﻿using TestModelBuilderExample.Models;

namespace TestModelBuilderExample.Services
{
    /// <summary>
    /// A simple class for use in demoing the Builder pattern for test models.
    /// </summary>
    public class BookingService
    {
        public bool ProcessBooking(Booking booking)
        {
            // here, we create a contrived example to show in the tests why we might want differing Booking instances
            if (booking.BookingReference.StartsWith("A") || booking.Surname == "Townsend")
            {
                return false;
            }

            // fall-through to here and return true (for default values)
            return true;
        }
    }
}